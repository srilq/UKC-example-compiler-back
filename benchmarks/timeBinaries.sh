touch results.csv
echo "bin,time" > results.csv

for i in {0..100}; do
    echo "---------------------------------------- Starting round: $i"
    for filename in testFiles/*; do
        echo -e "\tTesting file $filename"
        #/usr/bin/time --quiet -f "%C, %e" $filename 2>> results.csv >/dev/null
        TIME=$((time $filename > /dev/null) 2>&1 >/dev/null | grep real | cut -f 2 -d'm'| cut -f 1 -d's')
        #TIME=$((time $filename > /dev/null <./data.input) 2>&1 >/dev/null | grep real | cut -f 2 -d'm'| cut -f 1 -d's')
        echo $filename,$TIME >> results.csv
    done
done
