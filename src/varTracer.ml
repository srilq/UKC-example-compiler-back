module B = BlockStructure

type traceable = (B.var * B.atomic_exp * B.atomic_exp * bool)
type tracer = traceable list

let build_tracer_from_temp_vars (temp_vars : ((B.atomic_exp * Tokens.op * B.atomic_exp), B.var) Hashtbl.t) : tracer =
  let traceable_from_var (((e1,_,e2),v) : ((B.atomic_exp * Tokens.op * B.atomic_exp) * B.var)) : traceable =
    (v,e1,e2,true) (* var will always be needed the first time*)
  in (
    let pair_fun temp_vars = Hashtbl.fold (fun k v accum -> (k, v)::accum) temp_vars [] in
    let pairs = pair_fun temp_vars in
    (*let pair_no = "Number of temp_vars " ^ (string_of_int (List.length pairs)) in
    print_string pair_no;*)
    List.map traceable_from_var pairs
  )

(* alert all of the traceable elements that a variable has been changed
 * elements that use this variable will be set as updated to prevent the
 * elimination of their temp var assignment*)
let update_by_var (trace : tracer) (v : B.var) : tracer =
  let rec update_atomic (trace : tracer) (atomic : B.atomic_exp) : tracer =
    (match trace with
    | ((v,e1,e2,_)::ts) when e1==atomic -> ((v,e1,e2,true)::update_atomic ts atomic)
    | ((v,e1,e2,_)::ts) when e2==atomic -> ((v,e1,e2,true)::update_atomic ts atomic)
    | (t::ts) -> (t::update_atomic ts atomic)
    | _ -> []
    ) in
  let atomic = B.Ident(v) in
  update_atomic trace atomic

let contains (s1 : string) (s2 : string) : bool =
  let re = Str.regexp_string s2 in
  try ignore (Str.search_forward re s1 0); true
  with Not_found -> false

let is_needed (trace : tracer) (v : B.var) : bool =
  let rec safe_is_needed (trace : tracer) (v : B.var) : bool =
    (match trace with
    | ((var,_,_,b)::_) when var==v -> b
    | (_::ts) -> safe_is_needed ts v
    | _ -> raise Not_found
    ) in(
      match v with
      | B.NamedTmp(name,_) when (contains name "CSE")==true ->
        (*print_string ((B.show_var v) ^ " is a CSE evaling removal\n");*)
        safe_is_needed trace v
      | _ ->
        (*print_string ((B.show_var v) ^ " is not a CSE, it must stay\n");*)
        true (* if it is not one of our temp vars then it is needed *)
    )

let rec ack_update (trace : tracer) (v : B.var) : tracer =
  match v with
  | B.NamedTmp(name,_) when (contains name "CSE")==true -> (
    match trace with
    | ((var,e1,e2,_)::ts) when var==v -> ((var,e1,e2,false)::ts)
    | (t::ts) -> (t::(ack_update ts v))
    | _ -> raise Not_found
  )
  | _ -> trace
