module B = BlockStructure
module T = VarTracer

type se = (B.atomic_exp * Tokens.op * B.atomic_exp)

let contains_exp (cse : se) (ex : B.atomic_exp) : bool =
  match cse with
  | (e,_,_) when e == ex -> true
  | (_,_,e) when e == ex -> true
  | _ -> false

let rec print_all (bl : B.block_elem list) : unit =
  match bl with
  | (b::bs) ->
    B.pp_block_elem Format.std_formatter b;
    Format.pp_print_newline Format.std_formatter ();
    print_all bs
  | _ -> ()

let print_wrap (bl : B.block_elem list) : unit =
  Format.pp_print_newline Format.std_formatter ();
  print_all bl;
  Format.pp_print_newline Format.std_formatter ();
  ()

(* Returns the number of times a give cse has appeared in a Hashtbl*)
let num_cses (cses : (se, bool) Hashtbl.t) (cse :se) : int =
  List.length (Hashtbl.find_all cses cse)

(* Converts all subexpressions appearing more than once in assign operations to use temp vars
 * Expects a reversed list to be provided
 * Returns the block in reverse order *)
let rec se_to_temp_var (cses : (se, bool) Hashtbl.t) (temp_vars : (se, B.var) Hashtbl.t) (bl : B.block_elem list) : B.block_elem list =
  match bl with
  | (B.AssignOp(v,e1,o,e2)::bs) when (num_cses cses (e1,o,e2)) > 1 ->
    let sub_expression = (e1,o,e2) in
    (match Hashtbl.mem temp_vars sub_expression with (* Do we have a temp var for this expression*)
    | true ->
      let tmp_var = Hashtbl.find temp_vars sub_expression in
      let tmp_assignment = B.AssignAtom(v,B.Ident(tmp_var)) in
      let modified_assignment = B.AssignOp(tmp_var,e1,o,e2) in

      (tmp_assignment::modified_assignment::(se_to_temp_var cses temp_vars bs))
    | false ->
      let tmp_var = B.NamedTmp("CSE",(Hashtbl.length temp_vars)) in
      Hashtbl.add temp_vars sub_expression tmp_var;

      let tmp_var = Hashtbl.find temp_vars sub_expression in
      let tmp_assignment = B.AssignAtom(v,B.Ident(tmp_var)) in
      let modified_assignment = B.AssignOp(tmp_var,e1,o,e2) in

      (tmp_assignment::modified_assignment::(se_to_temp_var cses temp_vars bs)))
  | (b::bs) -> (b::(se_to_temp_var cses temp_vars bs))
  | _ -> []

(* Populates a given Hashtbl with subexpressions found in a basic block*)
let rec get_cses (cses : (se, bool) Hashtbl.t) (bl : B.block_elem list) : unit =
  match bl with
  | (B.AssignOp(_,e1,o,e2)::bs) ->
    Hashtbl.add cses (e1,o,e2) true;
    get_cses cses bs
  | (_::bs) ->
    get_cses cses bs
  | _ -> ()

let build_cses (bl : B.block_elem list) : (se, bool) Hashtbl.t =
  let cses = Hashtbl.create (List.length bl) in
  get_cses cses bl;
  cses

let rec trace_and_eliminate (trace : T.tracer) (bl : B.block_elem list) : B.block_elem list =
  match bl with
  | ((B.AssignOp(v,_,_,_) as elem)::bs) when (T.is_needed trace v)==true ->
    (*print_string ("Cannot remove " ^ (B.show_var v) ^ "\n");*)
    let updated_trace = T.ack_update (T.update_by_var trace v) v in
    (elem::(trace_and_eliminate updated_trace bs))

  | (B.AssignOp(v,_,_,_)::bs) when (T.is_needed trace v)==false ->
    (*print_string "Removing allocation\n";*)
    (trace_and_eliminate trace bs)

  | ((B.AssignAtom(v,_) as elem)::bs) ->
    let updated_trace = T.update_by_var trace v in
    (elem::(trace_and_eliminate updated_trace bs))

  | ((B.Ld(v,_,_) as elem)::bs) ->
    let updated_trace = T.update_by_var trace v in
    (elem::(trace_and_eliminate updated_trace bs))

  | ((B.Call(Some(v),_,_) as elem)::bs) ->
    let updated_trace = T.update_by_var trace v in
    (elem::(trace_and_eliminate updated_trace bs))

  | (b::bs) -> (b::(trace_and_eliminate trace bs))
  | [] -> []

let eliminate_cse (entry : B.cfg_entry) : B.cfg_entry =
  let basicBlock = entry.elems in
  let temp_vars = Hashtbl.create (List.length basicBlock) in
  let cses = build_cses basicBlock in
  let with_tmp_vars = List.rev (se_to_temp_var cses temp_vars (List.rev basicBlock)) in
  let var_tracer = T.build_tracer_from_temp_vars temp_vars in
  let eliminated = trace_and_eliminate var_tracer with_tmp_vars in

  print_wrap eliminated;
  { bnum=entry.bnum; elems=eliminated; next=entry.next; started=entry.started; finished=entry.finished }

let rec eliminate_cses (controlFlowGraph : B.cfg) : B.cfg =
  match controlFlowGraph with
  | (c::cs) -> (eliminate_cse c) :: (eliminate_cses cs)
  | _ -> []
