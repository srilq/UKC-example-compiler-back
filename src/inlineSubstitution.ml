open Util
open SourceAst

(*
  For each Call exp in a func body:
  - Look up corresponding func
  - Add new var_dec with Temp id, convert Call exp to Ident exp with the same id, store the id
  - Add new var_dec with Temp id for each var_dec in func
  - Make copy of func body
  - For each stmt in body:
    - Change each Ident exp in stmt to use corresponding id
    - Convert Return stmt to Assign stmt with id corresponding to original Call exp
    - Recurse for each Call exp in stmt (?)
    - Add stmt immediately before original Call exp
*)

let id_string (id : id) : string =
  match id with
  | Source (s, _) -> s
  | Temp (s, _) -> s

let typ_init (t : typ) : exp =
  let rec array_init (i : int) (es : exp list) : exp list =
    if i > 0 then (array_init (i - 1) (es @ [Num 0L])) else es
  in
  match t with
  | Int -> Num 0L
  | Bool -> Bool false
  | Array i -> Array (array_init i [])

let id_to_temp (s : string) (id : id) : id =
  Temp (s ^ "_" ^ id_string id, 0)

let var_dec_to_temp (s : string) (dec : var_dec) : var_dec =
  { dec with var_name = id_to_temp s dec.var_name }

let rec lookup_func (name : string) (fs : func list) : func =
  match fs with
  | [] -> raise (InternalError ("Error during inline substitution: function lookup failed for name: " ^ name))
  | (f::rest) -> if String.compare (id_string f.fun_name) name == 0 then f else lookup_func name rest

(*
 * Function body can only be inlined if:
 *  - They have no more than 1 return statement.
 *  - They do not have any call expressions.
*)
let is_inlineable_func_body (stmts : stmt list) : bool =
  let rec is_inlineable_exps (exps : exp list) : bool =
    let rec is_inlineable_e (exp : exp) : bool =
      match exp with
      | Ident (_,es) -> is_inlineable_exps es
      | Call (_,_) -> false
      | Op (e1,_,e2) -> is_inlineable_e e1 && is_inlineable_e e2
      | Uop (_,e) -> is_inlineable_e e
      | Array es -> is_inlineable_exps es
      | _ -> true
    in
    match exps with
    | [] -> true
    | (e::rest) -> is_inlineable_e e && is_inlineable_exps rest
  in
  let is_inlineable_exp (exp : exp) : bool =
    is_inlineable_exps [exp]
  in
  let rec is_inlineable_ss (ret_count : int) (sts : stmt list) : (bool * int) =
    let rec is_inlineable_stmt (ret_count : int) (stmt : stmt) : (bool * int) =
      match stmt with
      | Assign (_, es, e) ->
        (is_inlineable_exps es && is_inlineable_exp e, ret_count)
      | DoWhile (s1, e, s2) ->
        let (inlineable_s1, ret_count) = is_inlineable_stmt ret_count s1 in
        let (inlineable_s2, ret_count) = is_inlineable_stmt ret_count s2 in
        (inlineable_s1 && is_inlineable_exp e && inlineable_s2, ret_count)
      | Ite (e, s1, s2) ->
        let (inlineable_s1, ret_count) = is_inlineable_stmt ret_count s1 in
        let (inlineable_s2, ret_count) = is_inlineable_stmt ret_count s2 in
        (is_inlineable_exp e && inlineable_s1 && inlineable_s2, ret_count)
      | Stmts stmts ->
        let (inlineable, ret_count) = is_inlineable_ss ret_count stmts in
        (inlineable, ret_count)
      | Loc (s, _) ->
        is_inlineable_stmt ret_count s
      | Return _ -> (true, ret_count + 1)
      | _ -> (true, ret_count)
    in
    match sts with
    | [] -> (true, ret_count)
    | (s::rest) ->
      let (inlineable_s, ret_count) = is_inlineable_stmt ret_count s in
      let (inlineable_rest, ret_count) = is_inlineable_ss ret_count rest in
      (inlineable_s && inlineable_rest, ret_count)
  in
  let (inlineable, ret_count) = is_inlineable_ss 0 stmts in
  inlineable && ret_count <= 1

(*
 * Handles inlining for a list of statements.
 * Returns:
 *   a list of statements to replace the given statements list,
 *   a list of var_decs to be added to the holding function
*)
let rec inline_sub_stmts (fs : func list) (loc : int option) (stmts : stmt list) : (stmt list * var_dec list) =

  let inline_sub_func_body (ret_id : id) (body : stmt list) : stmt list =
    let rec inline_sub_fb (stmts : stmt list) : stmt list =
      let inline_sub_fb_id (id : id) : id =
        id_to_temp "INS_LOC" id (* NOTE: this is a fudge *)
      in
      let rec inline_sub_fb_exps (exps : exp list) : exp list =
        let rec inline_sub_fb_e (exp : exp) : exp =
          match exp with
          | Ident (id, es) -> Ident (inline_sub_fb_id id, inline_sub_fb_exps es)
          | Call (id, es) -> Call (inline_sub_fb_id id, inline_sub_fb_exps es)
          | Op (e1, op, e2) -> Op (inline_sub_fb_e e1, op, inline_sub_fb_e e2)
          | Uop (uop, e) -> Uop (uop, inline_sub_fb_e e)
          | Array (es) -> Array (inline_sub_fb_exps es)
          | _ -> exp
        in
        match exps with
        | [] -> []
        | (e::rest) -> [inline_sub_fb_e e] @ inline_sub_fb_exps rest
      in
      let inline_sub_fb_exp (exp : exp) : exp =
        List.nth (inline_sub_fb_exps [exp]) 0
      in
      let rec inline_sub_fb_stmt (stmt : stmt) : stmt =
        match stmt with
        | Assign (id, es, e) ->
          Assign (inline_sub_fb_id id, inline_sub_fb_exps es, inline_sub_fb_exp e)
        | DoWhile (s1, e, s2) ->
          DoWhile (inline_sub_fb_stmt s1, inline_sub_fb_exp e, inline_sub_fb_stmt s2)
        | Ite (e, s1, s2) ->
          Ite (inline_sub_fb_exp e, inline_sub_fb_stmt s1, inline_sub_fb_stmt s2)
        | Stmts stms -> Stmts (inline_sub_fb stms)
        | In id -> In (inline_sub_fb_id id)
        | Out id -> Out (inline_sub_fb_id id)
        | Return Some id -> Assign (ret_id, [], Ident (inline_sub_fb_id id, []))
        | Return None -> Assign (ret_id, [], Ident (inline_sub_fb_id ret_id, []))
        | Loc (s, l) -> Loc (s, l)
      in
      match stmts with
      | [] -> []
      | (s::rest) ->
        let s' = inline_sub_fb_stmt s in
        [s'] @ inline_sub_fb rest
    in
    inline_sub_fb body
  in

  let rec inline_sub_exps (exps : exp list) : (exp list * stmt list * var_dec list) =
    let rec inline_sub_e (exp : exp) : (exp * stmt list * var_dec list) =
      let rec params_to_var_decs (params : (id * typ) list) : var_dec list =
        match params with
        | [] -> []
        | (p::rest) ->
          let (id, typ) = p in
          let dec = { var_name = id_to_temp "INS_LOC" id; typ = typ; init = typ_init typ; loc = loc } in
          [dec] @ params_to_var_decs rest
      in
      let rec assign_params_args (args : exp list) (params : (id * typ) list) : stmt list =
        match params with
        | [] -> []
        | (p::rest_params) ->
          let (id, _) = p in
          match args with
          | [] -> raise (InternalError ("Error during inline substitution: missing function argument."))
          | (exp::rest_args) ->
            [Assign (id_to_temp "INS_LOC" id, [], exp)] @ assign_params_args rest_args rest_params
      in
      match exp with
      | Ident (id, es) ->
        let (es', stmts, decs) = inline_sub_exps es in
        (Ident (id, es'), stmts, decs)
      | Call (id, args) ->
        let (args', stmts_args, decs_args) = inline_sub_exps args in
        let func = lookup_func (id_string id) fs in
        if is_inlineable_func_body func.body
        then
          let id_ret = id_to_temp "INS_RET" id in
          let dec_ret = { var_name = id_ret; typ = func.ret; init = typ_init func.ret; loc = loc } in
          let decs_locals = List.map (var_dec_to_temp "INS_LOC") func.locals in
          let decs_params = params_to_var_decs func.params in
          let stmts_body = inline_sub_func_body id_ret func.body in
          let stmts_assign_params = assign_params_args args' func.params in
          (Ident (id_ret, []), stmts_args @ stmts_assign_params @ stmts_body, decs_args @ [dec_ret] @ decs_params @ decs_locals)
        else
          (Call (id, args'), stmts_args, decs_args)
      | Op (e1, op, e2) ->
        let (e1', stmts1, decs1) = inline_sub_e e1 in
        let (e2', stmts2, decs2) = inline_sub_e e2 in
        (Op (e1', op, e2'), stmts1 @ stmts2, decs1 @ decs2)
      | Uop (uop, e) ->
        let (e', stmts, decs) = inline_sub_e e in
        (Uop(uop, e'), stmts, decs)
      | Array es ->
        let (es', stmts, decs) = inline_sub_exps es in
        (Array es', stmts, decs)
      | _ -> (exp, [], [])
    in
    let rec inline_sub_es (es : exp list) (stmts : stmt list) (decs : var_dec list) : (exp list * stmt list * var_dec list) =
      match es with
      | [] -> (es, stmts, decs)
      | (e::rest) ->
        let (e', stmts_e, decs_e) = inline_sub_e e in
        let (es_rest, stmts_rest, decs_rest) = inline_sub_es rest [] [] in
        ([e'] @ es_rest, stmts_e @ stmts_rest, decs_e @ decs_rest)
    in
    inline_sub_es exps [] []
  in

  let inline_sub_exp (exp : exp) : (exp * stmt list * var_dec list) =
    let (exps, stmts, decs) = inline_sub_exps [exp] in
    (List.nth exps 0, stmts, decs)
  in

  let rec inline_sub_stmt (loc : int option) (stmt : stmt) : (stmt * stmt list * var_dec list) =
    match stmt with
    | Assign (id, es, e) ->
      let (e', stmts, decs) = inline_sub_exp e in
      let (es', stmts_es, decs_es) = inline_sub_exps es in
      let stmt' = Assign (id, es', e') in
      (stmt', stmts @ stmts_es, decs @ decs_es)
    | DoWhile (s1, e, s2) ->
      let (s1', stmts1, decs1) = inline_sub_stmt loc s1 in
      let (s2', stmts2, decs2) = inline_sub_stmt loc s2 in
      let (e', stmts, decs) = inline_sub_exp e in
      let stmt' = DoWhile (s1', e', s2') in
      let stmts' = stmts @ stmts1 @ stmts2 in
      let decs' = decs @ decs1 @ decs2 in
      (stmt', stmts', decs')
    | Ite (e, s1, s2) ->
      let (s1', stmts1, decs1) = inline_sub_stmt loc s1 in
      let (s2', stmts2, decs2) = inline_sub_stmt loc s2 in
      let (e', stmts, decs) = inline_sub_exp e in
      let stmt' = Ite (e', s1', s2') in
      let stmts' = stmts @ stmts1 @ stmts2 in
      let decs' = decs @ decs1 @ decs2 in
      (stmt', stmts', decs')
    | Stmts stmts ->
      let (stmts_inline, decs) = inline_sub_stmts fs loc stmts in
      (Stmts stmts_inline, [], decs)
    | Loc (s, i) ->
      let (s, stmts, ds) = inline_sub_stmt (Some i) s in
      (Loc (s, i), stmts, ds)
    | _ -> (stmt, [], [])
  in
  match stmts with
  | [] -> ([], [])
  | (stmt::rest) ->
    let (s, stmts, decs) = inline_sub_stmt loc stmt in
    let (stmts_rest, decs_rest) = inline_sub_stmts fs loc rest in
    (stmts @ [s] @ stmts_rest, decs @ decs_rest)

let inline_sub_function (fs : func list) (f : func) : func =
  let (body', locals_new) = inline_sub_stmts fs f.loc f.body in
  { f with locals = (f.locals @ locals_new); body = body' }

let inline_sub_prog (p : prog) : prog =
  let funcs' = List.map (inline_sub_function p.funcs) p.funcs in
  { p with funcs = funcs' }
