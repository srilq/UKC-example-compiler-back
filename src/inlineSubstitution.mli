(* Inline substitution/function inlining *)

val inline_sub_prog : SourceAst.prog -> SourceAst.prog
